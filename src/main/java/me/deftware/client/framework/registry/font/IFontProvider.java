package me.deftware.client.framework.registry.font;

import me.deftware.client.framework.fonts.legacy.LegacyBitmapFont;

/**
 * @author Deftware
 */
public interface IFontProvider {

	LegacyBitmapFont getFont();

}
